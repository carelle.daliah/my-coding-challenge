#!/bin/bash
# install nginx
sudo apt update
sudo apt-get -y install nginx

# make sure nginx is started
sudo systemctl start nginx
sudo systemctl enable nginx
