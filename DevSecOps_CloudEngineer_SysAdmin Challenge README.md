------------------------------- Challenge instructions -------------------------------

1. Create the server.

- update your server

sudo apt update

- install nginx

sudo apt-get -y install nginx

- start nginx

sudo systemctl start nginx

- start after reboot the server

sudo systemctl enable nginx 

2. Run the checker script you created.

- create a script with the command

 sudo vim serverup.sh

- insert the code and save

- if you are not a root give your user permission

 sudo chmod +x serverup.sh

- run the script

sudo ./serverup.sh

